"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var logger_1 = require("./utils/logger");
var canvas_1 = require("./canvas");
var engine_1 = require("./engine/engine");
var loggerObj = new logger_1.Logger(document.querySelector("#loggerContainer"), 20);
var icanvas = {
    fillStyle: "#000"
};
var canvasObj = new canvas_1.Canvas(document.querySelector("#canvasContainer canvas"), icanvas, loggerObj);
var simulationEngine = new engine_1.Engine(canvasObj, loggerObj);
var btnReset = document.querySelector("#btnReset");
var btnStart = document.querySelector("#btnStart");
var btnClearLog = document.querySelector("#btnClearLog");
var btnStartToggle = toggleButtonState(btnStart, { activeText: 'Stop', inactiveText: 'Start' });
btnReset.addEventListener("click", function (e) {
    simulationEngine.resetSimulation()
        .then(btnStartToggle)
        .catch(loggerObj.logError);
});
btnStart.addEventListener("click", function (e) {
    var _btnCtx = this;
    if (!!_btnCtx.getAttribute('data-status') && _btnCtx.getAttribute('data-status') == 'active') {
        simulationEngine.stopSimulation()
            .then(btnStartToggle)
            .catch(function (err) {
            loggerObj.logError(err);
            btnStartToggle(true);
        });
    }
    else {
        simulationEngine.startSimulation()
            .then(btnStartToggle)
            .catch(function (err) {
            loggerObj.logError(err);
            btnStartToggle(false);
        });
    }
});
btnClearLog.addEventListener("click", function (e) {
    loggerObj.clear();
});
function toggleButtonState(_btnCtx, _opt) {
    return function (_state) {
        _btnCtx.innerHTML = (_state ? _opt.activeText : _opt.inactiveText);
        _btnCtx.setAttribute('data-status', (_state ? 'active' : 'inactive'));
    };
}
