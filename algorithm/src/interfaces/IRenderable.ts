import { Canvas } from '../canvas';

export interface IRenderable {
    render(ctx: CanvasRenderingContext2D): void;
}