export class Point {
    constructor(private _x?: number, private _y?: number) {
        if (typeof this._x === "undefined") this._x = 0;
        if (typeof this._y === "undefined") this._y = 0;
    }

    public get x() {
        return this._x;
    }
    public set x(_x:number) {
        this._x = _x;
    }
    public get y() {
        return this._y;
    }
    public set y(_y:number) {
        this._y = _y;
    }
}