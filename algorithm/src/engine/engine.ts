import { Logger } from '../utils/logger';
import { Canvas } from '../canvas';
import { Node } from './node';
import { Interaction } from './interaction';

export class Engine {
    private _nodeList: Node[];
    private _nodeInteractions: Interaction[];
    private _canvas: Canvas;
    private _logger: Logger;
    private _timer: number;

    constructor(canvas: Canvas, logger: Logger, nodeList?: Node[]) {
        this._canvas = canvas;
        this._logger = logger;
        this._nodeList = nodeList || [];
        let ctx = this;
        this._nodeList.forEach((pNode:Node) => {
            ctx._nodeList.forEach((iNode:Node) => {
                if (iNode.id !== pNode.id) {
                    ctx._nodeInteractions.push({
                        id: pNode.id,
                        iid: iNode.id,
                        interaction: 0
                    });
                }
            });
        });
        this._logger.log("Simulation Engine Initilized");
    }

    private stepSimulate(ctx:Engine) {
        return function() {
            ctx._nodeList.forEach((node:Node)=>{
                let test = node.update();
                if (test !== false) {
                    ctx._nodeList.forEach((inode:Node)=>{
                        
                    });
                }
            });
            ctx._timer = requestAnimationFrame(ctx.stepSimulate(ctx));
        };
    }

    public startSimulation(): Promise<boolean> {
        return new Promise((resolve, reject) => {
            this._canvas.start()
            .then( status => {
                this._timer = requestAnimationFrame(this.stepSimulate(this));
                resolve(status);
            })
            .catch(reject);
        });
    }

    public stopSimulation(): Promise<boolean> {
        return new Promise((resolve, reject) => {
            this._canvas.stop()
            .then( status => {
                cancelAnimationFrame(this._timer);
                resolve(status);
            })
            .catch(reject);
        });
    }

    public resetSimulation(): Promise<boolean> {
        return new Promise((resolve, reject) => {
            this._canvas.resetCanvas()
            .then(status => {
                cancelAnimationFrame(this._timer);
                resolve(status);
            })
            .catch(reject);
        });
    }
};