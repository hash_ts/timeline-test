"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Engine = /** @class */ (function () {
    function Engine(canvas, logger, nodeList) {
        this._canvas = canvas;
        this._logger = logger;
        this._nodeList = nodeList || [];
        var ctx = this;
        this._nodeList.forEach(function (pNode) {
            ctx._nodeList.forEach(function (iNode) {
                if (iNode.id !== pNode.id) {
                    ctx._nodeInteractions.push({
                        id: pNode.id,
                        iid: iNode.id,
                        interaction: 0
                    });
                }
            });
        });
        this._logger.log("Simulation Engine Initilized");
    }
    Engine.prototype.stepSimulate = function (ctx) {
        return function () {
            ctx._nodeList.forEach(function (node) {
                var test = node.update();
                if (test !== false) {
                    ctx._nodeList.forEach(function (inode) {
                    });
                }
            });
            ctx._timer = requestAnimationFrame(ctx.stepSimulate(ctx));
        };
    };
    Engine.prototype.startSimulation = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this._canvas.start()
                .then(function (status) {
                _this._timer = requestAnimationFrame(_this.stepSimulate(_this));
                resolve(status);
            })
                .catch(reject);
        });
    };
    Engine.prototype.stopSimulation = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this._canvas.stop()
                .then(function (status) {
                cancelAnimationFrame(_this._timer);
                resolve(status);
            })
                .catch(reject);
        });
    };
    Engine.prototype.resetSimulation = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this._canvas.resetCanvas()
                .then(function (status) {
                cancelAnimationFrame(_this._timer);
                resolve(status);
            })
                .catch(reject);
        });
    };
    return Engine;
}());
exports.Engine = Engine;
;
