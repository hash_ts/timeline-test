"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Point = /** @class */ (function () {
    function Point(_x, _y) {
        this._x = _x;
        this._y = _y;
        if (typeof this._x === "undefined")
            this._x = 0;
        if (typeof this._y === "undefined")
            this._y = 0;
    }
    Object.defineProperty(Point.prototype, "x", {
        get: function () {
            return this._x;
        },
        set: function (_x) {
            this._x = _x;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Point.prototype, "y", {
        get: function () {
            return this._y;
        },
        set: function (_y) {
            this._y = _y;
        },
        enumerable: true,
        configurable: true
    });
    return Point;
}());
exports.Point = Point;
