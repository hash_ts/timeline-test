"use strict";
exports.__esModule = true;
var Logger = /** @class */ (function () {
    function Logger(_root, scrollSpeed) {
        if (typeof _root === "undefined") {
            this._root = document.createElement("div");
            this._root.setAttribute("style", "width:450px;height:320px;border:1px solid #777;margin:21px auto;");
            document.body.appendChild(this._root);
        }
        else {
            this._root = _root;
        }
        this._scrollSpeed = scrollSpeed || 50;
        this._root.addEventListener("mouseover", this.pauseScroll(this));
        this._root.addEventListener("mouseout", this.resumeScroll(this));
        this._reachedMaxScroll = false;
        this._previousScrollTop = 0;
        this._root.scrollTop = 0;
        this.resumeScroll(this)();
    }
    Logger.prototype.pauseScroll = function (ctx) {
        return function () {
            cancelAnimationFrame(ctx._timer);
        };
    };
    Logger.prototype.resumeScroll = function (ctx) {
        return function () {
            ctx._previousScrollTop = ctx._root.scrollTop;
            ctx._timer = requestAnimationFrame(ctx.scrollDiv(ctx));
        };
    };
    Logger.prototype.scrollDiv = function (ctx) {
        return function () {
            ctx._reachedMaxScroll = ctx._root.scrollTop >= (ctx._root.scrollHeight - ctx._root.offsetHeight);
            if (!ctx._reachedMaxScroll) {
                ctx._root.scrollTop = ctx._previousScrollTop;
                ctx._previousScrollTop++;
            }
            ctx._timer = requestAnimationFrame(ctx.scrollDiv(ctx));
        };
    };
    Logger.prototype.log = function (message) {
        var messageElement = document.createElement("p");
        var wrapperObj = document.createElement("div");
        wrapperObj.setAttribute("style", "padding:0px 10px 5px;font-family:sans-serif;font-size:10px");
        messageElement.innerHTML = "Logger [" + (new Date().toLocaleString()) + "]: <b>" + message + "</b>";
        wrapperObj.appendChild(messageElement);
        this._root.appendChild(wrapperObj);
    };
    return Logger;
}());
exports.Logger = Logger;
