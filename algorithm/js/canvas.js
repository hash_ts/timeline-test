"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var logger_1 = require("./utils/logger");
var Canvas = /** @class */ (function () {
    function Canvas(_root, options, logger) {
        if (typeof _root === "undefined") {
            throw new Error("Canvas Element Not Found");
        }
        if (typeof logger === "undefined") {
            this._logger = new logger_1.Logger();
        }
        else {
            this._logger = logger;
        }
        this._options = options;
        this._objectList = [];
        this._root = _root;
        this._root.width = parseInt(window.getComputedStyle(this._root).getPropertyValue("width"));
        this._root.height = parseInt(window.getComputedStyle(this._root).getPropertyValue("height"));
        this._width = parseInt(window.getComputedStyle(this._root).getPropertyValue("width"));
        this._height = parseInt(window.getComputedStyle(this._root).getPropertyValue("height"));
        window.addEventListener("resize", this.resizeCompensate(this));
        this.init();
    }
    Canvas.prototype.resizeCompensate = function (ctx) {
        return function (e) {
            ctx._width = parseInt(window.getComputedStyle(ctx._root).getPropertyValue("width"));
            ctx._height = parseInt(window.getComputedStyle(ctx._root).getPropertyValue("height"));
            ctx._root.width = parseInt(window.getComputedStyle(ctx._root).getPropertyValue("width"));
            ctx._root.height = parseInt(window.getComputedStyle(ctx._root).getPropertyValue("height"));
            ctx.init(true);
            ctx._logger.log("Canvas Resized");
        };
    };
    Canvas.prototype.update = function (ctx) {
        return function () {
            ctx._objectList.forEach(function (element) {
                element.render(ctx._ctx);
            });
            ctx._timer = requestAnimationFrame(ctx.update(ctx));
        };
    };
    Canvas.prototype.init = function (noLog) {
        this._ctx = this._root.getContext("2d");
        this._ctx.clearRect(0, 0, this._width, this._height);
        this._ctx.fillStyle = this._options.fillStyle;
        this._ctx.fillRect(0, 0, this._width, this._height);
        if (typeof noLog === "undefined" || noLog === false)
            this._logger.log("Canvas Initialized");
    };
    Canvas.prototype.start = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this._timer = requestAnimationFrame(_this.update(_this));
            _this._logger.log("Canvas Simulation Started");
            // this._logger.log("Raising Error");
            // reject(new Error("Test Error"));
            resolve(true);
        });
    };
    Canvas.prototype.stop = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            cancelAnimationFrame(_this._timer);
            _this._logger.log("Canvas Simulation Stopped");
            resolve(false);
        });
    };
    Canvas.prototype.pushObj = function (obj) {
        this._objectList.push(obj);
    };
    Canvas.prototype.resetCanvas = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            cancelAnimationFrame(_this._timer);
            _this.init(true);
            _this._logger.log("Canvas Reset");
            resolve(false);
        });
    };
    return Canvas;
}());
exports.Canvas = Canvas;
