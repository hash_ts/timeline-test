var TimelineComponent = /** @class */ (function () {
    function TimelineComponent(id, name, date, active, date_end, is_present, show_delta) {
        this._id = id;
        this._name = name || "";
        this._date = date || null;
        this._date_end = date_end || date;
        this._show_delta = show_delta || (this._date_end == this._date ? false : true);
        this._is_present = is_present || (this._date_end == this._date ? true : false);
        this._present_text = 'Present';
        this._renderedNode = null;
        this._active = active || false;
        this._evt = function (e) { };
        this._hook = function () { };
        return this;
    }
    TimelineComponent.prototype.updateDom = function () {
        if (this._active) {
            this._renderedNode.querySelector('.event-node').classList.add('active');
        }
        else {
            this._renderedNode.querySelector('.event-node').classList.remove('active');
        }
    };
    Object.defineProperty(TimelineComponent.prototype, "active", {
        set: function (tf) {
            this._active = tf;
            this._hook();
            if (tf) {
                this._evt({ data: this._id });
            }
            this.updateDom();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TimelineComponent.prototype, "name", {
        get: function () {
            return this._name;
        },
        set: function (name) {
            this._name = name;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TimelineComponent.prototype, "date", {
        get: function () {
            return this._date;
        },
        set: function (date) {
            this._date = date;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TimelineComponent.prototype, "present_text", {
        get: function () {
            return this._present_text;
        },
        set: function (str) {
            this._present_text = str;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TimelineComponent.prototype, "date_end", {
        get: function () {
            return this._date_end;
        },
        set: function (date) {
            this._date_end = date;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TimelineComponent.prototype, "event", {
        set: function (ev) {
            if (typeof ev === 'function') {
                this._evt = ev;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TimelineComponent.prototype, "hook", {
        set: function (ev) {
            if (typeof ev === 'function') {
                this._hook = ev;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TimelineComponent.prototype, "id", {
        get: function () {
            return this._id;
        },
        enumerable: true,
        configurable: true
    });
    TimelineComponent.prototype.preRender = function () {
        var wrapper, nodeWrapper, eventNode, eventName, dateNode;
        wrapper = document.createElement('div');
        wrapper.classList.add("event-wrapper");
        nodeWrapper = document.createElement('div');
        nodeWrapper.classList.add('node-wrapper');
        eventNode = document.createElement('span');
        eventNode.classList.add('event-node');
        eventName = document.createElement('span');
        eventName.classList.add('event-name');
        eventName.innerText = this.name;
        dateNode = document.createElement('span');
        dateNode.classList.add('date-wrapper');
        dateNode.innerText = this._show_delta ? this.date.getFullYear() + '-' + (this._is_present ? this.present_text : this._date_end.getFullYear()) : this.date.toDateString();
        nodeWrapper.append(document.createElement('hr'));
        nodeWrapper.append(eventNode);
        nodeWrapper.append(document.createElement('hr'));
        wrapper.append(dateNode);
        wrapper.append(nodeWrapper);
        wrapper.append(eventName);
        this._renderedNode = wrapper;
        this.updateDom();
        return this;
    };
    Object.defineProperty(TimelineComponent.prototype, "node", {
        get: function () {
            return this._renderedNode.querySelector('.event-node');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TimelineComponent.prototype, "wrapper", {
        get: function () {
            return this._renderedNode;
        },
        enumerable: true,
        configurable: true
    });
    TimelineComponent.prototype.attach = function (_parent) {
        _parent.appendChild(this._renderedNode);
    };
    return TimelineComponent;
}());
var Timeline = /** @class */ (function () {
    function Timeline(_root, initObject, _fn) {
        this.timelineComponents = new Array();
        this._gliders = new Array();
        this._root = _root;
        this._currOffset = 0;
        this._currActive = 0;
        this._globalTimer = null;
        this._globalFn = (typeof _fn === 'function') ? _fn : function (e) { };
        if (initObject) {
            try {
                this.generateFromObject(initObject);
            }
            catch (e) {
                console.error(e.message);
            }
        }
        return this;
    }
    Timeline.prototype.resetTimelineComponents = function () {
        this._root.querySelectorAll('.event-node').forEach(function (element) {
            element.classList.remove('active');
        });
    };
    Timeline.prototype.animate = function (sign, offset) {
        var _self = this;
        function looper() {
            this._globalTimer = requestAnimationFrame(looper.bind(this));
            var now = Date.now(), delta = now - this._then;
            if (delta > this._interval) {
                this._then = now - (delta % this._interval);
                this._currOffset = (sign * (this._currOffset + sign * delta) > sign * offset ? offset : this._currOffset + sign * delta);
                this._dom.style.transform = "translateX(" + this._currOffset + "px)";
                if (this._currOffset == offset)
                    cancelAnimationFrame(this._globalTimer);
            }
        }
        looper.bind(this)();
    };
    Timeline.prototype.shiftComponents = function () {
        var i = this._currActive - 1;
        if (i == -1 || i >= this._timelineComponents.length - 2) {
            if (i == -1)
                this._gliders[0].obj.classList.add('disabled');
            else
                this._gliders[1].obj.classList.add('disabled');
            return;
        }
        this._gliders.forEach(function (g) { return g.obj.classList.remove('disabled'); });
        var pW = this._dom.offsetWidth, pOL = this._dom.offsetLeft, bOL = this._timelineComponents[0].wrapper.offsetLeft, cOL = this._timelineComponents[i].wrapper.offsetLeft, cW = this._timelineComponents[i].wrapper.offsetWidth;
        var offset = (0 - cOL - pOL + bOL), diff = offset - this._currOffset;
        //let offset = (0 - cOL - pOL + cW + 3*bOL - pW);
        var sign = isNaN(diff / Math.abs(diff)) ? 1 : diff / Math.abs(diff);
        this._then = Date.now();
        this._interval = 1000 / 120;
        this.animate(sign, offset);
    };
    Timeline.prototype.moveLeft = function () {
        this.resetTimelineComponents();
        this._currActive = this._currActive + 1;
        this._timelineComponents[this._currActive].active = true;
    };
    Timeline.prototype.moveRight = function () {
        this.resetTimelineComponents();
        this._currActive = this._currActive - 1;
        this._timelineComponents[this._currActive].active = true;
    };
    Timeline.prototype.right_fn = function (_self) {
        var _timer = null;
        return function (e) {
            if (_timer !== null)
                return;
            _timer = true;
            if (_self._currActive >= (_self._timelineComponents.length - 1)) {
                console.log(_self._currActive);
                _timer = null;
                return;
            }
            _self.moveLeft.bind(_self)();
            setTimeout(function () { return _timer = null; }, 250);
        };
    };
    Timeline.prototype.left_fn = function (_self) {
        var _timer = null;
        return function (e) {
            if (_timer !== null)
                return;
            _timer = true;
            if (_self._currActive <= 0) {
                console.log(_self._currActive);
                _timer = null;
                return;
            }
            _self.moveRight.bind(_self)();
            setTimeout(function () { return _timer = null; }, 250);
        };
    };
    Timeline.prototype.initEventListeners = function () {
        var _self = this;
        this._timelineComponents.forEach(function (component) {
            component.node.addEventListener('click', function (e) {
                _self.resetTimelineComponents();
                _self._currActive = _self._timelineComponents.findIndex(function (c) { return c.id === component.id; });
                component.active = true;
            });
        });
        this._gliders.forEach(function (glider) {
            glider.obj.addEventListener('click', glider.fn(_self));
        });
    };
    Timeline.prototype.generateFromObject = function (initObject) {
        var _this = this;
        if (initObject.length == 0) {
            throw new Error("Failed to Instantiate from empty Object");
        }
        else {
            initObject.sort(function (e1, e2) {
                return (e1.date > e2.date) ? 1 : -1;
            }).forEach(function (initComponentObj) {
                var tcObj = new TimelineComponent(initComponentObj.id, initComponentObj.name, initComponentObj.date, initComponentObj.active, initComponentObj.date_end, initComponentObj.is_present, initComponentObj.show_delta);
                if (initComponentObj.present_text !== undefined) {
                    tcObj.present_text = initComponentObj.present_text;
                }
                tcObj.event = initComponentObj.evt || _this._globalFn;
                tcObj.hook = _this.shiftComponents.bind(_this);
                _this.push(tcObj);
            });
        }
    };
    Timeline.prototype.attachGlider = function () {
        var gliderLeft = document.createElement('span');
        var gliderRight = document.createElement('span');
        gliderLeft.classList.add('slider', 'slide-lt');
        gliderRight.classList.add('slider', 'slide-rt');
        gliderLeft.innerText = "<";
        gliderRight.innerText = ">";
        this._root.append(gliderLeft);
        this._root.append(gliderRight);
        this._gliders.push({ fn: this.left_fn, obj: gliderLeft });
        this._gliders.push({ fn: this.right_fn, obj: gliderRight });
    };
    Object.defineProperty(Timeline.prototype, "timelineComponents", {
        set: function (objs) {
            this._timelineComponents = Array.from(objs);
        },
        enumerable: true,
        configurable: true
    });
    Timeline.prototype.push = function (obj) {
        this._timelineComponents.push(obj);
        return this;
    };
    Timeline.prototype.pop = function (obj) {
        return this._timelineComponents.pop();
    };
    Timeline.prototype.render = function () {
        var _this = this;
        this._dom = document.createElement('div');
        this._dom.classList.add('timeline-wrapper');
        this._timelineComponents.forEach(function (component) {
            component.preRender().attach(_this._dom);
        });
        this._root.append(this._dom);
        this.attachGlider();
        this.initEventListeners();
        // Set 1st entry as selected if not selected
        if (this._timelineComponents.findIndex(function (e) { return e.active; }) == -1) {
            this._timelineComponents[0].active = true;
        }
    };
    return Timeline;
}());
