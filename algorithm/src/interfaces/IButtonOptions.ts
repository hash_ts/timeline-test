export interface IButtonOptions {
    activeText: string;
    inactiveText: string;
}