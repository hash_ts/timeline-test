import { IRenderable } from '../interfaces/IRenderable';
import { Point } from '../utils/point';
import { IObjectOptions } from '../interfaces/IObjectOptions';

export class Circle implements IRenderable {
    private _pt: Point;
    private _radius: number;
    private _options: IObjectOptions;

    constructor(_radius: number, pt: Point, options: IObjectOptions) {
        this._radius = (_radius > 0) ? _radius : 0;
        this._pt = pt;
        this._options = options;
    }

    public render(ctx:CanvasRenderingContext2D) {
        ctx.beginPath();
        ctx.fillStyle = this._options.fillStyle;
        ctx.lineWidth = this._options.lineWidth;
        ctx.strokeStyle = this._options.fillStyle;

        ctx.arc(this._pt.x, this._pt.y, this._radius, 0, 2*Math.PI, false);

        if(this._options.stroke) ctx.stroke();
        if (this._options.fill) ctx.fill();

        ctx.closePath();
    }
}