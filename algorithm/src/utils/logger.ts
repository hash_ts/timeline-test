export class Logger {
    private _root: Element;
    private _timer: number;
    private _previousScrollTop: number;
    private _reachedMaxScroll: boolean;
    private _scrollSpeed: number;

    constructor(_root?: Element, scrollSpeed?: number) {
        if (typeof _root === "undefined") {
            this._root = document.createElement("div");
            this._root.setAttribute("style", "width:450px;height:320px;border:1px solid #777;margin:21px auto;");
            document.body.appendChild(this._root);
        } else {
            this._root = _root;
        }
        this._scrollSpeed = scrollSpeed || 50;
        this._root.addEventListener("mouseover", this.pauseScroll(this));
        this._root.addEventListener("mouseout", this.resumeScroll(this));
        this._reachedMaxScroll = false;
        this._previousScrollTop = 0;
        this._root.scrollTop = 0;
        this.resumeScroll(this)();
    }

    private pauseScroll(ctx) {
        return function() {
            cancelAnimationFrame(ctx._timer);
        }
    }

    private resumeScroll(ctx) {
        return function() {
            ctx._previousScrollTop = ctx._root.scrollTop;
            ctx._timer = requestAnimationFrame(ctx.scrollDiv(ctx));
        }
    }

    private scrollDiv(ctx) {
        return function() {
            ctx._reachedMaxScroll = ctx._root.scrollTop >= (ctx._root.scrollHeight - (<HTMLElement>ctx._root).offsetHeight);
            if (!ctx._reachedMaxScroll) {
                ctx._root.scrollTop = ctx._previousScrollTop;
                ctx._previousScrollTop++;
            }
            ctx._timer = requestAnimationFrame(ctx.scrollDiv(ctx));
        }
    }

    private logBody(message: string, isError: boolean) {
        let messageElement = document.createElement("p");
        let wrapperObj = document.createElement("div");
        wrapperObj.setAttribute("style","padding:0px 10px 5px;font-family:sans-serif;font-size:10px");
        if (isError) {
            wrapperObj.style.color = '#e46';
            wrapperObj.style.fontWeight = 'bold';
        }
        messageElement.innerHTML = "Logger [" + (new Date().toLocaleString()) + "]: <b>" + message + "</b>";
        wrapperObj.appendChild(messageElement);

        this._root.appendChild(wrapperObj);
    }
    
    public log(message: string) {
        this.logBody(message, false);
    }

    public logError(err: DOMException) {
        this.logBody(err.message, true);
    }

    public clear() {
        this._root.innerHTML = "";
        this.logBody("Log Window Cleared", false);
    }
}