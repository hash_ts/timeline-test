import { Logger } from './utils/logger';
import { Canvas } from './canvas';
import { ICanvasOptions } from './interfaces/ICanvasOptions';
import { IObjectOptions } from './interfaces/IObjectOptions';
import { IButtonOptions } from './interfaces/IButtonOptions';
import { Circle } from './objects/circle';
import { Point } from './utils/point';
import { Engine } from './engine/engine';

let loggerObj = new Logger(document.querySelector("#loggerContainer"), 20);
let icanvas: ICanvasOptions = {
    fillStyle: "#000"
};
let canvasObj = new Canvas(document.querySelector("#canvasContainer canvas"), icanvas, loggerObj);

let simulationEngine = new Engine(canvasObj, loggerObj);

let btnReset = document.querySelector("#btnReset");
let btnStart = document.querySelector("#btnStart");
let btnClearLog = document.querySelector("#btnClearLog");

let btnStartToggle = toggleButtonState(btnStart, { activeText: 'Stop', inactiveText: 'Start' });

btnReset.addEventListener("click", function(e) {
    simulationEngine.resetSimulation()
    .then(btnStartToggle)
    .catch(loggerObj.logError);
});

btnStart.addEventListener("click", function(e) {
    let _btnCtx = <Element>this;
    if (!!_btnCtx.getAttribute('data-status') && _btnCtx.getAttribute('data-status') == 'active') {
        simulationEngine.stopSimulation()
        .then(btnStartToggle)
        .catch(err => {
            loggerObj.logError(err);
            btnStartToggle(true);
        });
    } else {
        simulationEngine.startSimulation()
        .then(btnStartToggle)
        .catch(err => {
            loggerObj.logError(err);
            btnStartToggle(false);
        });
    }
});

btnClearLog.addEventListener("click", function(e) {
    loggerObj.clear();
});

function toggleButtonState(_btnCtx: Element, _opt: IButtonOptions) {
    return function(_state: boolean) {
        _btnCtx.innerHTML = (_state ? _opt.activeText : _opt.inactiveText);
        _btnCtx.setAttribute('data-status', (_state ? 'active' : 'inactive'));
    }
}
