(function() {
    var _fn = function(_id) {
        return function(data) {
            var g = document.querySelectorAll('#' + _id + ' .g');
            g.forEach(function(e) {
                e.classList.add('hidden');
            });
            Array.from(g).filter(function(e) {
                return data.data == e.getAttribute('data-id');
            }).forEach(function(m) {
                m.classList.remove('hidden');
            });
        };
    };
    var initObj = [{
            id: 'obj1',
            name: 'High School',
            date: new Date('10 May 2006'),
            date_end: new Date('12 May 2008'),
        },
        {
            id: 'obj2',
            name: 'Metriculation',
            date: new Date('15 May 2008'),
            date_end: new Date('10 May 2010'),
        },
        {
            id: 'obj3',
            name: 'Graduation',
            date: new Date('30 July 2010'),
            date_end: new Date('30 April 2014'),
        },
        {
            id: 'obj4',
            name: 'Post Graduation',
            date: new Date('30 July 2014'),
            date_end: new Date('12 May 2016'),
        },
        {
            id: 'obj5',
            name: 'PhD',
            date: new Date('31 August 2016'),
            show_delta: true
        }
    ];

    var timeline = new Timeline(document.getElementById('timeline-container'), initObj, _fn('gc'));
    timeline.render();
})();