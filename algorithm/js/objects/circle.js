"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Circle = /** @class */ (function () {
    function Circle(_radius, pt, options) {
        this._radius = (_radius > 0) ? _radius : 0;
        this._pt = pt;
        this._options = options;
    }
    Circle.prototype.render = function (ctx) {
        ctx.beginPath();
        ctx.fillStyle = this._options.fillStyle;
        ctx.lineWidth = this._options.lineWidth;
        ctx.strokeStyle = this._options.fillStyle;
        ctx.arc(this._pt.x, this._pt.y, this._radius, 0, 2 * Math.PI, false);
        if (this._options.stroke)
            ctx.stroke();
        if (this._options.fill)
            ctx.fill();
        ctx.closePath();
    };
    return Circle;
}());
exports.Circle = Circle;
