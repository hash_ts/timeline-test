import { Point } from '../utils/point';
import { Logger } from '../utils/logger';
import { Topics } from './topics';

export class Node {
    private _id: number;
    private _pt: Point;
    private _logger: Logger;
    private _topics: Topics[];
    
    constructor(id:number, position: Point, logger: Logger) {
        this._id = id;
        this._pt = position;
        this._logger = logger;
        this._logger.log("Node with id: #" + id + " created.");
    }

    get id() {
        return this._id;
    }

    get topics() {
        return this._topics;
    }

    set topic(topic: Topics) {
        this._topics.push(topic);
    }

    set topicList(topics: Topics[]) {
        this._topics = topics;
    }

    public update(): Topics|boolean {
        if ((Math.random() >= 0.3)) return this._topics[Math.floor(Math.random()*this._topics.length)];
        return false;
    }

    public interact(inode:Node ,interactivity:number) : number {
        if (Math.random() > 0.7 || interactivity > 0.9) {
            
        }
        return 0;
    }
}