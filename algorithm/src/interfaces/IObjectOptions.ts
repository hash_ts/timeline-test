export interface IObjectOptions {
    fillStyle: any;
    lineWidth: number;
    fill: boolean;
    stroke: boolean;
}