interface InitObject {
    id: string,
    name: string,
    date: Date,
    date_end?: Date,
    is_present?: boolean,
    show_delta?: boolean,
    active?: boolean,
    present_text?: string,
    evt?: any,
}

interface SliderObject {
    fn: any,
    obj: any
}

class TimelineComponent {
    private _id: string;
    private _name: string;
    private _date: Date;
    private _date_end: Date;
    private _active: boolean;
    private _present_text: string;
    private _show_delta: boolean;
    private _is_present: boolean;
    private _renderedNode: HTMLElement;
    private _evt;
    private _hook;

    private updateDom() {
        if (this._active) {
            this._renderedNode.querySelector('.event-node').classList.add('active');
        } else {
            this._renderedNode.querySelector('.event-node').classList.remove('active');
        }
    }

    constructor(id, name?, date?, active?, date_end?, is_present?, show_delta?) {
        this._id = id;
        this._name = name || "";
        this._date = date || null;
        this._date_end = date_end || date;
        this._show_delta = show_delta || (this._date_end==this._date ? false : true);
        this._is_present = is_present || (this._date_end==this._date ? true : false);
        this._present_text = 'Present';
        this._renderedNode = null;
        this._active = active || false;
        this._evt = (e) => {};
        this._hook = () => {};
        return this;
    }

    set active(tf:boolean) {
        this._active = tf;
        this._hook();
        if (tf) {
            this._evt({ data: this._id });
        }
        this.updateDom();
    }

    set name(name: string) {
        this._name = name;
    }

    set date(date: Date) {
        this._date = date;
    }

    set present_text(str: string) {
        this._present_text = str;
    }

    set date_end(date: Date) {
        this._date_end = date;
    }

    set event(ev: any) {
        if (typeof ev === 'function') {
            this._evt = ev;
        }
    }

    set hook(ev: any) {
        if (typeof ev === 'function') {
            this._hook = ev;
        }
    }

    get id(): string {
        return this._id;
    }

    get date(): Date {
        return this._date;
    }

    get date_end(): Date {
        return this._date_end;
    }

    get name(): string {
        return this._name;
    }

    get present_text() {
        return this._present_text;
    }

    preRender(): TimelineComponent {
        let wrapper, nodeWrapper, eventNode, eventName, dateNode;
        wrapper = document.createElement('div');
        wrapper.classList.add("event-wrapper");
        nodeWrapper = document.createElement('div');
        nodeWrapper.classList.add('node-wrapper');
        eventNode = document.createElement('span');
        eventNode.classList.add('event-node');
        eventName = document.createElement('span');
        eventName.classList.add('event-name');
        eventName.innerText = this.name;
        dateNode = document.createElement('span');
        dateNode.classList.add('date-wrapper');
        dateNode.innerText = this._show_delta ? this.date.getFullYear()+'-'+(this._is_present ? this.present_text : this._date_end.getFullYear()) : this.date.toDateString();
        nodeWrapper.append(document.createElement('hr'));
        nodeWrapper.append(eventNode);
        nodeWrapper.append(document.createElement('hr'));
        wrapper.append(dateNode);
        wrapper.append(nodeWrapper);
        wrapper.append(eventName);
        this._renderedNode = wrapper;
        this.updateDom();
        return this;
    }

    get node() {
        return this._renderedNode.querySelector('.event-node');
    }

    get wrapper() {
        return this._renderedNode;
    }

    attach(_parent: HTMLElement): void {
        _parent.appendChild(this._renderedNode);
    }
}

class Timeline {
    private _timelineComponents: Array<TimelineComponent>;
    private _gliders: Array<SliderObject>;
    private _root: any;
    private _dom: any;
    private _currOffset: number;
    private _currActive: number;
    private _globalFn: any;
    private _globalTimer: any;
    private _then: number;
    private _interval: number;

    private resetTimelineComponents() {
        this._root.querySelectorAll('.event-node').forEach(element => {
            element.classList.remove('active'); 
        });
    }

    private animate(sign, offset) {
        let _self = this;
        function looper() {
            this._globalTimer = requestAnimationFrame(looper.bind(this));
            let now = Date.now(),
                delta = now - this._then;

            if (delta > this._interval) {
                this._then = now - (delta % this._interval);
                this._currOffset = (sign*(this._currOffset + sign*delta) > sign*offset ? offset : this._currOffset + sign*delta);
                this._dom.style.transform = "translateX("+this._currOffset+"px)";
                if (this._currOffset == offset) cancelAnimationFrame(this._globalTimer);
            }
        }
        looper.bind(this)();
    }

    private shiftComponents() {
        let i = this._currActive-1;
        if (i == -1 || i >= this._timelineComponents.length-2) {
            if (i == -1) this._gliders[0].obj.classList.add('disabled');
            else this._gliders[1].obj.classList.add('disabled');
            return;
        }
        this._gliders.forEach((g)=>g.obj.classList.remove('disabled'));
        let pW = this._dom.offsetWidth,
            pOL = this._dom.offsetLeft,
            bOL = this._timelineComponents[0].wrapper.offsetLeft,
            cOL = this._timelineComponents[i].wrapper.offsetLeft,
            cW = this._timelineComponents[i].wrapper.offsetWidth;
        let offset = (0 - cOL - pOL + bOL),
            diff = offset - this._currOffset;
        //let offset = (0 - cOL - pOL + cW + 3*bOL - pW);
        let sign = isNaN(diff/Math.abs(diff)) ? 1 : diff/Math.abs(diff);
        this._then = Date.now();
        this._interval = 1000/120;
        this.animate(sign, offset);
    }

    private moveLeft() {
        this.resetTimelineComponents();
        this._currActive = this._currActive + 1;
        this._timelineComponents[this._currActive].active = true;
    }
    private moveRight() {
        this.resetTimelineComponents();
        this._currActive = this._currActive - 1;
        this._timelineComponents[this._currActive].active = true;
    }

    private right_fn(_self) {
        let _timer = null
        return (e) => {
            if (_timer !== null) return;
            _timer = true;
            if (_self._currActive >= (_self._timelineComponents.length-1)) {
                console.log(_self._currActive);
                _timer = null;
                return;
            }
            _self.moveLeft.bind(_self)();
            setTimeout(()=>_timer=null,250);
        }
    }
    private left_fn(_self) {
        let _timer = null;
        return (e) => {
            if (_timer !== null) return;
            _timer = true;
            if (_self._currActive <= 0) {
                console.log(_self._currActive);
                _timer = null;
                return;
            }
            _self.moveRight.bind(_self)();
            setTimeout(()=>_timer=null,250);
        }
    }

    private initEventListeners(): void {
        let _self = this;
        this._timelineComponents.forEach((component) => {
            component.node.addEventListener('click', (e) => {
                _self.resetTimelineComponents();
                _self._currActive = _self._timelineComponents.findIndex((c)=>c.id===component.id);
                component.active = true;
            });
        });
        this._gliders.forEach((glider) => {
            glider.obj.addEventListener('click', glider.fn(_self));
        });
    }

    private generateFromObject(initObject: Array<InitObject>): void {
        if (initObject.length == 0) {
            throw new Error("Failed to Instantiate from empty Object");
        } else {
            initObject.sort((e1, e2) => { 
                return (e1.date>e2.date)?1:-1;
            }).forEach((initComponentObj) => {
                let tcObj = new TimelineComponent(
                    initComponentObj.id, 
                    initComponentObj.name, 
                    initComponentObj.date,
                    initComponentObj.active,
                    initComponentObj.date_end,
                    initComponentObj.is_present,
                    initComponentObj.show_delta
                );
                if (initComponentObj.present_text !== undefined) {
                    tcObj.present_text = initComponentObj.present_text;
                }
                tcObj.event = initComponentObj.evt || this._globalFn;
                tcObj.hook = this.shiftComponents.bind(this);
                this.push(tcObj);
            });
        }
    }

    private attachGlider() {
        let gliderLeft = document.createElement('span');
        let gliderRight = document.createElement('span');
        gliderLeft.classList.add('slider','slide-lt');
        gliderRight.classList.add('slider','slide-rt');
        gliderLeft.innerText = "<";
        gliderRight.innerText = ">";
        this._root.append(gliderLeft);
        this._root.append(gliderRight);
        this._gliders.push({fn:this.left_fn, obj: gliderLeft});
        this._gliders.push({fn: this.right_fn, obj: gliderRight});
    }

    constructor(_root: HTMLElement, initObject?: Array<InitObject>, _fn?: any) {
        this.timelineComponents = new Array<TimelineComponent>();
        this._gliders = new Array<SliderObject>();
        this._root = _root;
        this._currOffset = 0;
        this._currActive = 0;
        this._globalTimer = null;
        this._globalFn = (typeof _fn === 'function') ? _fn : function(e) {};
        if (initObject) {
            try {
                this.generateFromObject(initObject);
            } catch (e) {
                console.error(e.message);
            }
        }
        return this;
    }

    set timelineComponents(objs: Array<TimelineComponent>) {
        this._timelineComponents = Array.from(objs);
    }

    push(obj: TimelineComponent): Timeline {
        this._timelineComponents.push(obj);
        return this;
    }

    pop(obj: TimelineComponent): TimelineComponent {
        return this._timelineComponents.pop();
    }

    render() {
        this._dom = document.createElement('div');
        this._dom.classList.add('timeline-wrapper');
        this._timelineComponents.forEach(component => {
            component.preRender().attach(this._dom);
        });
        this._root.append(this._dom);
        this.attachGlider();
        this.initEventListeners();
        // Set 1st entry as selected if not selected
        if (this._timelineComponents.findIndex((e)=>e.active) == -1) {
            this._timelineComponents[0].active = true;
        }
    }
}