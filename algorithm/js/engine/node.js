"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Node = /** @class */ (function () {
    function Node(id, position, logger) {
        this._id = id;
        this._pt = position;
        this._logger = logger;
        this._logger.log("Node with id: #" + id + " created.");
    }
    Object.defineProperty(Node.prototype, "id", {
        get: function () {
            return this._id;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Node.prototype, "topics", {
        get: function () {
            return this._topics;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Node.prototype, "topic", {
        set: function (topic) {
            this._topics.push(topic);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Node.prototype, "topicList", {
        set: function (topics) {
            this._topics = topics;
        },
        enumerable: true,
        configurable: true
    });
    Node.prototype.update = function () {
        if ((Math.random() >= 0.3))
            return this._topics[Math.floor(Math.random() * this._topics.length)];
        return false;
    };
    Node.prototype.interact = function (inode, interactivity) {
        if (Math.random() > 0.7 || interactivity > 0.9) {
        }
        return 0;
    };
    return Node;
}());
exports.Node = Node;
