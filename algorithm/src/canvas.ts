import { Logger } from "./utils/logger";
import { ICanvasOptions } from "./interfaces/ICanvasOptions";
import { IRenderable } from "./interfaces/IRenderable";

export class Canvas {
  private _root: HTMLCanvasElement;
  private _logger: Logger;
  private _options: ICanvasOptions;
  private _ctx: CanvasRenderingContext2D;
  private _width: number;
  private _height: number;
  private _timer: number;
  private _objectList: IRenderable[];

  constructor(
    _root: HTMLCanvasElement,
    options: ICanvasOptions,
    logger?: Logger
  ) {
    if (typeof _root === "undefined") {
      throw new Error("Canvas Element Not Found");
    }
    if (typeof logger === "undefined") {
      this._logger = new Logger();
    } else {
      this._logger = logger;
    }
    this._options = options;
    this._objectList = [];
    this._root = _root;
    this._root.width = parseInt(
      window.getComputedStyle(this._root).getPropertyValue("width")
    );
    this._root.height = parseInt(
      window.getComputedStyle(this._root).getPropertyValue("height")
    );
    this._width = parseInt(
      window.getComputedStyle(this._root).getPropertyValue("width")
    );
    this._height = parseInt(
      window.getComputedStyle(this._root).getPropertyValue("height")
    );
    window.addEventListener("resize", this.resizeCompensate(this));
    this.init();
  }

  private resizeCompensate(ctx) {
    return e => {
      ctx._width = parseInt(
        window.getComputedStyle(ctx._root).getPropertyValue("width")
      );
      ctx._height = parseInt(
        window.getComputedStyle(ctx._root).getPropertyValue("height")
      );
      ctx._root.width = parseInt(
        window.getComputedStyle(ctx._root).getPropertyValue("width")
      );
      ctx._root.height = parseInt(
        window.getComputedStyle(ctx._root).getPropertyValue("height")
      );
      ctx.init(true);
      ctx._logger.log("Canvas Resized");
    };
  }

  private update(ctx) {
    return () => {
      ctx._objectList.forEach((element: IRenderable) => {
        element.render(ctx._ctx);
      });
      ctx._timer = requestAnimationFrame(ctx.update(ctx));
    };
  }

  private init(noLog?: boolean) {
    this._ctx = this._root.getContext("2d");
    this._ctx.clearRect(0, 0, this._width, this._height);
    this._ctx.fillStyle = this._options.fillStyle;
    this._ctx.fillRect(0, 0, this._width, this._height);
    if (typeof noLog === "undefined" || noLog === false)
      this._logger.log("Canvas Initialized");
  }

  public start(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this._timer = requestAnimationFrame(this.update(this));
      this._logger.log("Canvas Simulation Started");
      // this._logger.log("Raising Error");
      // reject(new Error("Test Error"));
      resolve(true);
    });
  }

  public stop(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      cancelAnimationFrame(this._timer);
      this._logger.log("Canvas Simulation Stopped");
      resolve(false);
    });
  }

  public pushObj(obj: IRenderable) {
    this._objectList.push(obj);
  }

  public resetCanvas(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      cancelAnimationFrame(this._timer);
      this.init(true);
      this._logger.log("Canvas Reset");
      resolve(false);
    });
  }
}
