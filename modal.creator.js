(function(window, document, $) {
    "use strict";
    var openPopup = function(dataObj) {
        var modal = $('<div class="modal fade" tabindex="-1" role="dialog"></div>');
        var modalDialog = $('<div class="modal-dialog" role="document"></div>');
        var modalContent = $('<div class="modal-content"></div>');
        var modalHeader = $('<div class="modal-header"></div>');
        var modalCloseBtn = $('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
        var modalTitle = $('<h4 class="modal-title">Modal title</h4>');
        var modalBody = $('<div class="modal-body"></div>');
        var modalFooter = $('<div class="modal-footer"></div>');
        var modalFooterCloseBtn = $('<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>');
        var modalFooterAcceptBtn = $('<button type="button" id="btn-modal-accept" class="btn btn-primary">Save changes</button>');

        modalCloseBtn.appendTo(modalHeader);
        modalTitle.html(dataObj.title);
        modalTitle.appendTo(modalHeader);
        modalHeader.appendTo(modalContent);
        modalBody.html(dataObj.body);
        modalBody.appendTo(modalContent);
        if (dataObj.needFooter) {
            if (dataObj.needDismissBtn) {
                modalFooterCloseBtn.html(dataObj.dismissBtnText);
                modalFooterCloseBtn.appendTo(modalFooter);
            }
            modalFooterAcceptBtn.html(dataObj.acceptBtnText);
            modalFooterAcceptBtn.appendTo(modalFooter);
            modalFooter.appendTo(modalContent);
        }
        modalContent.appendTo(modalDialog);
        modalDialog.appendTo(modal);

        return modal;
    };

    function alertlogin() {
        var dataObj = {
            "title": 'Login or Sign Up',
            "body": 'Please Login to follow penners from around the world',
            "needFooter": true,
            "acceptBtnText": "OK",
            "acceptCallback": function(that) {
                return function() {
                    that.remove();
                };
            }
        };
        openPopup(dataObj).modal('show').on('shown.bs.modal', function(e) {
            $('#btn-modal-accept').on('click', dataObj.acceptCallback($(this)));
        }).on('hidden.bs.modal', function(e) {
            $(this).remove();
        });
    }

})(window, document, jQuery);