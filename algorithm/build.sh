#!/bin/bash
tsc ./src/main.ts --outDir ./js/ --target "ES5" --lib "es5","dom","es2015.promise","es2015.iterable"
webpack --mode production --entry ./js/main.js --output-filename bundle.js
