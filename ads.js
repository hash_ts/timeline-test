(function(window, document, vjs, undefined) {
    "use strict";

    var registerPlugin = vjs.registerPlugin || vjs.plugin;

    registerPlugin("exampleAds", function(options) {
        var player = this,
            state = {},
            adServerUrl = (options && options.adServerUrl) || "inventory.json",
            midrollPoint = (options && options.midrollPoint) || [15],
            playPreroll = options && options.playPreroll !== undefined ? options.playPreroll : true,
            playMidroll = options && options.playMidroll !== undefined ? options.playMidroll : true,
            playPostroll = options && options.playPostroll !== undefined ? options.playPostroll : true,

            requestAds = function() {
                state = {};

                var xhr = new XMLHttpRequest();
                xhr.open("GET", adServerUrl + "?src=" + encodeURIComponent(player.currentSrc()));
                xhr.onreadystatechange = function() {
                    if (xhr.readyState === 4) {
                        try {
                            state.inventory = JSON.parse(xhr.responseText);
                            player.trigger('adsready');
                        } catch (err) {
                            throw new Error("Couldn't inventory response as JSON");
                        }
                    }
                };
                xhr.send(null);
            },

            playAd = function() {
                if (!state.inventory || state.inventory.length === 0) {
                    vjs.log("No inventory to play.");
                    return;
                }

                player.ads.startLinearAdMode();
                state.adPlaying = true;

                var media = state.inventory[Math.floor(Math.random() * state.inventory.length)];
                player.src(media);

                player.one("adended", function() {
                    player.ads.endLinearAdMode();
                    state.adPlaying = false;
                });
            },

            skipAd = function() {
                player.ads.endLinearAdMode();
                state.adPlaying = false;
            };

        player.ads(options);

        player.on("contentupdate", requestAds);

        if (player.currentSrc()) {
            requestAds();
        }

        player.on("contentended", function() {
            if (!state.postrollPlayed && player.ads.state === 'postroll?' && playPostroll) {
                state.postrollPlayed = true;
                playAd();
            }
        });

        player.on("readyforpreroll", function() {
            if (!state.prerollPlayed && playPreroll) {
                state.prerollPlayed = true;
                playAd();
            }
        });

        player.on('timeupdate', function(event) {
            if (state.midrollPlayed === (midrollPoint.length || 1)) {
                return;
            }

            var currentTime = player.currentTime(),
                opportunity,
                mrp = (midrollPoint[(state.midrollPlayed || 0)] || midrollPoint);

            if ('lastTime' in state) {
                opportunity = currentTime > mrp && state.lastTime < mrp;
            }

            state.lastTime = currentTime;
            if (opportunity && playMidroll) {
                state.midrollPlayed = (state.midrollPlayed !== undefined ? state.midrollPlayed + 1 : 1);
                playAd();
            }
        });

        var skipButton = document.getElementById("skipBtn");
        skipButton.addEventListener("click", function(e) {
            skipAd();
        });
    });

})(window, document, videojs);